const express = require("express");
const cors = require("cors");
const errorHandler=require('./middleware/erroHandeler')
const db = require("./app/models");
const userRoutes = require("./app/routes/user.routes");
const notificationRoutes = require("./app/routes/notification.routes");
const jobRoutes = require("./app/routes/job.routes");
const myNetworkRoutes = require("./app/routes/myNetwork.routes");
const messageRoutes = require("./app/routes/message.routes");
const personRoutes = require("./app/routes/person.routes");

const app = express();

const PORT = process.env.PORT || 8080;


app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Home route");
});

db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("connected to database");
  })
  .catch((err) => {
    console.log("Can not connected to database", { extended: true });
  });

userRoutes(app);
notificationRoutes(app);
jobRoutes(app);
myNetworkRoutes(app);
messageRoutes(app);
personRoutes(app);

app.use(errorHandler)

app.listen(PORT, () => {
  console.log("Server is running on port 8080");
});

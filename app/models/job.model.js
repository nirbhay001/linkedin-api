module.exports = (mongoose) => {
  let schema = mongoose.Schema(
    {
      job: String,
      company: String,
      profession: String,
      imgUrl: String,
      location: String,
      noOfJob: Number,
      date: String,
    },
    { timestamps: true }
  );
  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Job = mongoose.model("job", schema);
  return Job;
};

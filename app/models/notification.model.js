module.exports = (mongoose) => {
  let schema = mongoose.Schema(
    {
      name: String,
      company: String,
      imgUrl: String,
      postText: String,
      location: String,
      time: String,
    },
    { timestamps: true }
  );
  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Notification = mongoose.model("notification", schema);
  return Notification;
};

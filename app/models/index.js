const userModel = require("./user.model");
const notificationModel = require("./notification.model");
const jobModel = require("./job.model");
const myNetworkModel = require("./myNetwork.model");
const messageModel = require("./message.model");
const personModel = require("./person.model");
const dbConfig = require("../config/db.config");
const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const db = {};

db.url = dbConfig.url;
db.mongoose = mongoose;
db.users = userModel(mongoose);
db.notifications = notificationModel(mongoose);
db.jobs = jobModel(mongoose);
db.messages = messageModel(mongoose);
db.myNetworks = myNetworkModel(mongoose);
db.persons = personModel(mongoose);

module.exports = db;

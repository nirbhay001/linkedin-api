module.exports = (mongoose) => {
  let schema = mongoose.Schema(
    {
      name: String,
      userName: String,
      profession: String,
      company: String,
      location: String,
      college: String,
      follower: Number,
      connection: Number,
      imgUrl: String,
      about: String,
      experience: String,
      certification: Array,
      skills: Array,
      likes: Number,
      country: String,
    },
    { timestamps: true }
  );
  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const person = mongoose.model("person", schema);
  return person;
};

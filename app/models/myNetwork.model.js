module.exports = (mongoose) => {
  let schema = mongoose.Schema(
    {
      name: String,
      company: String,
      imgUrl: String,
      position: String,
      connection: Number,
      pages: Number,
      contact: Number,
      follower: Number,
      group: Number,
      events: Number,
      newsLetters: Number,
      hashTag: Number,
    },
    { timestamps: true }
  );
  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const myNetwork = mongoose.model("myNetwork", schema);
  return myNetwork;
};

module.exports = (mongoose) => {
  let schema = mongoose.Schema(
    {
      name: String,
      company: String,
      profession: String,
      imgUrl: String,
      message: String,
      date: String,
    },
    { timestamps: true }
  );
  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const message = mongoose.model("message", schema);
  return message;
};

module.exports = (mongoose) => {
  let schema = mongoose.Schema(
    {
      name: String,
      userName: String,
      profession: String,
      company: String,
      imgUrl: String,
      postText: String,
      likes: Number,
      country: String,
      comment: Array,
      time: String,
    },
    { timestamps: true }
  );
  schema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const User = mongoose.model("user", schema);
  return User;
};

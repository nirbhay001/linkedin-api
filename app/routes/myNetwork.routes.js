module.exports = (app) => {
  const myNetwork = require("../controllers/myNetwork.controller");
  let router = require("express").Router();

  router.post("/", myNetwork.create);

  router.get("/", myNetwork.findAll);

  router.get("/:id", myNetwork.findOne);

  router.put("/:id", myNetwork.update);

  router.delete("/:id", myNetwork.delete);

  router.delete("/", myNetwork.deleteAll);

  app.use("/api/myNetwork", router);
};

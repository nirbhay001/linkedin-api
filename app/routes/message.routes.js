module.exports = (app) => {
  const message = require("../controllers/message.controller");
  let router = require("express").Router();

  router.post("/", message.create);

  router.get("/", message.findAll);

  router.get("/:id", message.findOne);

  router.put("/:id", message.update);

  router.delete("/:id", message.delete);

  router.delete("/", message.deleteAll);

  app.use("/api/message", router);
};

const db = require("../models");
const jobCollection = db.jobs;

exports.create = (req, res, next) => {
  if (
    !req.body.job ||
    !req.body.company ||
    !req.body.profession ||
    !req.body.imgUrl ||
    !req.body.location ||
    !req.body.date
  ) {
    next({
      status: 400,
      message:
        "Content can not be empty, Enter name, profrssion, userName, company, postText and country",
    });
    return;
  }

  const job = new jobCollection({
    job: req.body.job,
    company: req.body.company,
    profession: req.body.profession,
    imgUrl: req.body.imgUrl,
    location: req.body.location,
    noOfJob: req.body.noOfJob,
    date: req.body.date,
  });
  job
    .save(job)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occured while creating the job.",
      });
    });
};

exports.findAll = (req, res, next) => {
  const name = req.query.name;
  var condition = name
    ? { name: { $regex: new RegExp(name), $options: "i" } }
    : {};

  jobCollection
    .find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error while retrieving job",
      });
    });
};

exports.findOne = (req, res, next) => {
  const id = req.params.id;
  jobCollection
    .findById(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: "Not found job with id " + id,
        });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error retrieving job with id=" + id,
      });
    });
};

exports.update = (req, res, next) => {
  if (!req.body) {
    return next({
      status: 400,
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  jobCollection
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot update job with id=${id}. Maybe job was not found!`,
        });
      } else res.send({ message: "job was updated successfully." });
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error updating job with id=" + id,
      });
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;

  jobCollection
    .findByIdAndRemove(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot delete job with id=${id}. Maybe job was not found!`,
        });
      } else {
        res.send({
          message: "job was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Could not delete job with id=" + id,
      });
    });
};

exports.deleteAll = (req, res, next) => {
  jobCollection
    .deleteMany({})
    .then((data) => {
      res.send({
        message: `${data.deletedCount} jobs were deleted successfully!`,
      });
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occurred while removing all jobs.",
      });
    });
};

const db = require("../models");
const messageCollection = db.messages;

exports.create = (req, res, next) => {
  if (
    !req.body.name ||
    !req.body.company ||
    !req.body.profession ||
    !req.body.imgUrl
  ) {
    next({
      status: 400,
      message: "Content can not be empty. Enter name, company, profession and image url"
    });
  }

  const message = new messageCollection({
    name: req.body.name,
    company: req.body.company,
    profession: req.body.profession,
    imgUrl: req.body.imgUrl,
    message: req.body.message,
    date: req.body.date,
  });
  message
    .save(message)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occured while creating the message.",
      });
    });
};

exports.findAll = (req, res, next) => {
  const name = req.query.name;
  var condition = name
    ? { name: { $regex: new RegExp(name), $options: "i" } }
    : {};

  messageCollection
    .find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error while retrieving message",
      });
    });
};

exports.findOne = (req, res, next) => {
  const id = req.params.id;
  messageCollection
    .findById(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: "Not found message with id " + id,
        });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error retrieving message with id=" + id,
      });
    })
};

exports.update = (req, res, next) => {
  if (!req.body) {
    return next({
      status: 400,
      message: "Data to update can not be empty!"
  });
  }

  const id = req.params.id;

  messageCollection
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot update message with id=${id}. Maybe message was not found!`
      });
      } else res.send({ message: "message was updated successfully." });
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error updating message with id=" + id
    });
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;

  messageCollection
    .findByIdAndRemove(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot delete message with id=${id}. Maybe message was not found!`
      });
      } else {
        res.send({
          message: "Message was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Could not delete message with id=" + id
    });
    });
};

exports.deleteAll = (req, res, next) => {
  messageCollection
    .deleteMany({})
    .then((data) => {
      res.send({
        message: `${data.deletedCount} Message were deleted successfully!`,
      });
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occurred while removing all message."
    });
    });
};

const db = require("../models");
const notificationCollection = db.notifications;

exports.create = (req, res, next) => {
  if (
    !req.body.name ||
    !req.body.company ||
    !req.body.imgUrl ||
    !req.body.location
  ) {
    next({
      status: 400,
      message:
        "Content can not be empty, Enter name, profrssion, userName, company, postText and country",
    });
    return;
  }

  const notification = new notificationCollection({
    name: req.body.name,
    company: req.body.company,
    imgUrl: req.body.imgUrl,
    postText: req.body.postText,
    location: req.body.location,
    time: req.body.string,
  });
  notification
    .save(notification)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message:
          err.message || "Some error occured while creating the notification.",
      });
    });
};

exports.findAll = (req, res, next) => {
  const name = req.query.name;
  var condition = name
    ? { name: { $regex: new RegExp(name), $options: "i" } }
    : {};

  notificationCollection
    .find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error while retrieving notification",
      });
    });
};

exports.findOne = (req, res, next) => {
  const id = req.params.id;
  notificationCollection
    .findById(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: "Not found notification with id " + id,
        });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error retrieving notification with id=" + id,
      });
    });
};

exports.update = (req, res, next) => {
  if (!req.body) {
    return next({
      status: 400,
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  notificationCollection
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot update notification with id=${id}. Maybe notification was not found!`,
        });
      } else res.send({ message: "notification was updated successfully." });
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error updating notification with id=" + id,
      });
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;

  notificationCollection
    .findByIdAndRemove(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot delete notification with id=${id}. Maybe notification was not found!`,
        });
      } else {
        res.send({
          message: "notification was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Could not delete notification with id=" + id,
      });
    });
};

exports.deleteAll = (req, res, next) => {
  notificationCollection
    .deleteMany({})
    .then((data) => {
      res.send({
        message: `${data.deletedCount} notifications were deleted successfully!`,
      });
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occurred while removing all notifications."
    });
    });
};

exports.findAllPublished = (req, res, next) => {
  notificationCollection
    .find({ published: true })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message:
          err.message ||
          "Some error occurred while removing all notifications.",
      });
    });
};

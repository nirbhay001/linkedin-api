const db = require("../models");
const userCollection = db.users;

exports.create = (req, res, next) => {
  if (
    (!req.body.name ||
      !req.body.profession ||
      !req.body.userName ||
      !req.body.company ||
      !req.body.imgUrl,
    !req.body.postText || !req.body.country)
  ) {
    next({
      status: 400,
      message:
        "Content can not be empty, enter name, profession, userName, company, postText and country",
    });
    return;
  }

  const user = new userCollection({
    name: req.body.name,
    userName: req.body.userName,
    profession: req.body.profession,
    company: req.body.company,
    imgUrl: req.body.imgUrl,
    postText: req.body.postText,
    country: req.body.country,
    likes: req.body.likes,
    comment: req.body.comment,
    time: req.body.time,
  });
  user
    .save(user)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occured while creating the user.",
      });
    });
};

exports.findAll = (req, res, next) => {
  const name = req.query.name;
  var condition = name
    ? { name: { $regex: new RegExp(name), $options: "i" } }
    : {};

  userCollection
    .find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error while retrieving user",
      });
    });
};

exports.findOne = (req, res, next) => {
  const id = req.params.id;
  userCollection
    .findById(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: "Not found user with id " + id,
        });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error retrieving user with id=" + id,
      });
    });
};

exports.update = (req, res, next) => {
  if (!req.body) {
    return next({
      status: 400,
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  userCollection
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot update user with id=${id}. Maybe user was not found!`,
        });
      } else res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error updating user with id=" + id,
      });
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;

  userCollection
    .findByIdAndRemove(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot delete user with id=${id}. Maybe user was not found!`,
        });
      } else {
        res.send({
          message: "user was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Could not delete user with id=" + id,
      });
    });
};

exports.deleteAll = (req, res, next) => {
  userCollection
    .deleteMany({})
    .then((data) => {
      res.send({
        message: `${data.deletedCount} Users were deleted successfully!`,
      });
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occurred while removing all users.",
      });
    });
};

const db = require("../models");
const personCollection = db.persons;

exports.create = (req, res, next) => {
  if (
    (!req.body.name ||
      !req.body.profession ||
      !req.body.company ||
      !req.body.imgUrl,
    !req.body.postText || !req.body.country)
  ) {
    next({
      status: 400,
      message:
        "Content can not be empty, Enter name, profrssion, company,image url, postText and country",
    });
    return;
  }

  const person = new personCollection({
    name: req.body.name,
    userName: req.body.userName,
    profession: req.body.profession,
    company: req.body.company,
    location: req.body.location,
    college: req.body.college,
    follower: req.body.follower,
    connection: req.body.connection,
    imgUrl: req.body.imgUrl,
    about: req.body.about,
    experience: req.body.experience,
    certification: req.body.certification,
    skills: req.body.skills,
    likes: req.body.lokes,
    country: req.body.country,
  });
  person
    .save(person)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occured while creating the person.",
      });
    });
};

exports.findAll = (req, res, next) => {
  const name = req.query.name;
  var condition = name
    ? { name: { $regex: new RegExp(name), $options: "i" } }
    : {};

  personCollection
    .find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error while retrieving person",
      });
    });
};

exports.findOne = (req, res, next) => {
  const id = req.params.id;
  personCollection
    .findById(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: "Not found person with id " + id,
        });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error retrieving person with id=" + id,
      });
    });
};

exports.update = (req, res, next) => {
  if (!req.body) {
    return next({
      status: 400,
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  personCollection
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot update person with id=${id}. Maybe person was not found!`,
        });
      } else res.send({ message: "person was updated successfully." });
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error updating person with id=" + id,
      });
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;

  personCollection
    .findByIdAndRemove(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot delete person with id=${id}. Maybe person was not found!`,
        });
      } else {
        res.send({
          message: "person was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Could not delete person with id=" + id,
      });
    });
};

exports.deleteAll = (req, res, next) => {
  personCollection
    .deleteMany({})
    .then((data) => {
      res.send({
        message: `${data.deletedCount} person were deleted successfully!`,
      });
    })
    .catch((err) => {
      next({
        status: 500,
        message:
          err.message || "Some error occurred while removing all persons.",
      });
    });
};

const db = require("../models");
const myNetworksCollection = db.myNetworks;

exports.create = (req, res, next) => {
  if (
    !req.body.name ||
    !req.body.company ||
    !req.body.imgUrl ||
    !req.body.position ||
    !req.body.connection ||
    !req.body.follower
  ) {
    next({
      status: 400,
      message:
        "Content can not be empty, Enter name, company, image url, postText,connection and follower",
    });
    return;
  }

  const myNetwork = new myNetworksCollection({
    name: req.body.name,
    company: req.body.company,
    imgUrl: req.body.imgUrl,
    position: req.body.position,
    connection: req.body.connection,
    pages: req.body.pages,
    contact: req.body.contact,
    follower: req.body.follower,
    group: req.body.group,
    events: req.body.events,
    newsLetters: req.body.newsLetters,
    hashTag: req.body.hashTag,
  });
  myNetwork
    .save(myNetwork)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occured while creating the my-network.",
      });
    });
};

exports.findAll = (req, res, next) => {
  const name = req.query.name;
  var condition = name
    ? { name: { $regex: new RegExp(name), $options: "i" } }
    : {};

  myNetworksCollection
    .find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error while retrieving my-network",
      });
    });
};

exports.findOne = (req, res, next) => {
  const id = req.params.id;
  myNetworksCollection
    .findById(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: "Not found my-network with id " + id,
        });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error retrieving my-network with id=" + id,
      });
    });
};

exports.update = (req, res, next) => {
  if (!req.body) {
    return next({
      status: 400,
      message: "Data to update can not be empty!"
  });
  }

  const id = req.params.id;

  myNetworksCollection
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot update my-network with id=${id}. Maybe my-network was not found!`
      });
      } else res.send({ message: "my-network was updated successfully." });
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Error updating my-network with id=" + id
    });
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;

  myNetworksCollection
    .findByIdAndRemove(id)
    .then((data) => {
      if (!data) {
        next({
          status: 404,
          message: `Cannot delete my-network with id=${id}. Maybe my-network was not found!`
      });
      } else {
        res.send({
          message: "myNetwork was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      next({
        status: 500,
        message: "Could not delete my-network with id=" + id
    });
    });
};

exports.deleteAll = (req, res, next) => {
  myNetworksCollection
    .deleteMany({})
    .then((data) => {
      res.send({
        message: `${data.deletedCount} my-network were deleted successfully!`,
      });
    })
    .catch((err) => {
      next({
        status: 500,
        message: err.message || "Some error occurred while removing all my-networks."
    });
    });
};
